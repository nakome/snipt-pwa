# Snipt pwa experiment

[Demo](https://snip-42d87.firebaseapp.com/)

The propouse is only for test, if you use in Production use under your responsibility.


### Dependencies:

- [Preact](https://preactjs.com/)  
- [Preact-router](https://git.io/preact-router)
- [Firebase](https://firebase.google.com/)
- [Marked](https://github.com/chjj/marked)
- [Ace](https://ace.c9.io/)
- Icons made with [Gravit Designer ](https://www.designer.io/)

### Notes 
You Can enter with Google account or Github account.
if you like Github account you need add in main.js the button like

	<button class="btn" onClick={() => Utils.githubSignin()}>
		{Utils.lang('Login with Github')}
	</button>

Be sure to add aplication in Firebase.

### Firebase config

Open src/config.js and in config_firebase add your firebase data.


### Rules for Firebase

	{
	  "rules": {
		 ".read":  "auth.uid != null",
		 "$uid":{
			"note":{
			  ".read": "auth.uid == $uid",
			  ".write":"auth.uid == $uid",
			  ".validate": "newData.isString()"
			},
			"idea":{
			  ".read": "auth.uid == $uid",
			  ".write":"auth.uid == $uid",
			   ".validate": "newData.isString()"
			},
			"snippets":{
			  ".read": "auth.uid == $uid",
			  "$key":{
				".write": "auth.uid == $uid && data.child('title').val() != newData.exists()",
				".validate": "newData.child('title').isString() && newData.child('desc').isString()",
				".indexOn": ".value"
			  }
			}
		 }
	  }
	}


### Intall dependencies

`npm install`

`npm start`

Go to `localhost:3000`


### build

**type:** `npm run build`









