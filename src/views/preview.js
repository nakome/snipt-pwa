import { h, render, Component } from 'preact';
import Match from 'preact-router/match';
import { route } from 'preact-router';
// utils
import Utils from '../controllers/utils';

// components
import ContentNavHeader from '../components/contentNavHeader';
import ContentFooter from '../components/contentfooter';


export default class Preview extends Component {
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {}
    };
    /**
     * Receive props
     */
    componentWillReceiveProps(nextProps) {
        Utils.firebase().ref(Utils.user('uid') + '/snippets/' + nextProps.uid).on('value', item => {
            let data = item.val();
            this.setState(data);
            this.setState({ content: Utils.lang('infoLoaded') });
            Utils.sleep(500)
                .then(() => {
                    if (data) {
                        this.setState({ content: marked(data.content) });
                        Utils.sleep(50).then(() => {
                            Utils.shortCodeParse(Utils.$('.content'));
                        });
                    }
                })
        });
    }
    /**
     * On mount
     */
    componentDidMount() {
        Utils.firebase().ref(Utils.user('uid') + '/snippets/' + this.props.uid).on('value', item => {
            let data = item.val();
            this.setState(data);
            this.setState({ content: Utils.lang('infoLoaded') });
            Utils.sleep(500)
                .then(() => {
                    if (data) {
                        this.setState({ content: marked(data.content) });
                        Utils.sleep(50).then(() => {
                            Utils.shortCodeParse(Utils.$('.content'));
                        });
                    }
                });
        });
    }
    /**
     * render
     */
    render({},{content}){
        return (
            <div id="content" class="main-content">
                <ContentNavHeader  uid={this.props.uid}/>
                <div class="content" dangerouslySetInnerHTML={{ __html: content }} />
                <ContentFooter  category={this.state.category} date={this.state.date}/>
            </div>
        );
    }
}