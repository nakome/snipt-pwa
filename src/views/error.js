import { h, render, Component } from 'preact';
import Match from 'preact-router/match';
import { route } from 'preact-router';
// utils
import Utils from '../controllers/utils';

export default class Error404 extends Component {

    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {
            content: ''
        }
    };
    /**
     * Gets the section..
     */
    getSection(el) {
        return route(el);
    }
    /**
     * Toggle info
     */
    toggleInfo() {
        Utils.$('.wrapper').classList.toggle('info-is-visible')
    }
    /**
     * Toggle sidebar
     */
    toggleSidebar() {
        Utils.$('.wrapper').classList.toggle('list-is-visible');
    }
    /**
     * On mount
     */
    componentDidMount() {
        if (Utils.userUid) {
            this.setState({ content: 'loading....' });
            Utils.sleep(500)
                .then(() => {
                    this.setState({
                        content: '<h1>404 Error</h1>'
                    });
                })
        }
    }
    /**
     * Render
     */
    render(){
        return (
            <div id="content" class="main-content">
                <div class="nav-header">
                  <nav>
                    <span class="pull-left">
                      <button onClick={() => this.toggleSidebar()} class="o-list" title="Open list"><i class="icon icon-arrow"></i></button>
                    </span>
                    <span class="pull-right">
                      <button onClick={() => this.getSection('/')}><i class="icon icon-home"></i></button>
                    </span>
                  </nav>
                </div>
                <div class="content" dangerouslySetInnerHTML={{ __html: this.state.content}}/>
                <div class="main-footer" id="footer"></div>
            </div>
        );
    }
}