import { h, render, Component } from 'preact';
import Match from 'preact-router/match';
import { route } from 'preact-router';
// utils
import Utils from '../controllers/utils';

export default class Home extends Component {
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {
            publicSnippets: [],
            note: Utils.lang('infoLoaded'),
            idea: Utils.lang('infoLoaded')
        }
    };
    /**
     * Gets the section.
     *
     */
    getSection(el) {
        return route(el);
    }
    /**
     * Toggle info
     */
    toggleInfo() {
        Utils.$('.wrapper').classList.toggle('info-is-visible')
    }
    /**
     * Toggle sidebar
     */
    toggleSidebar() {
        Utils.$('.wrapper').classList.toggle('list-is-visible');
    }
    /**
     * Sets the note.
     */
    setNote(e) {
        this.setState({ note: e.target.textContent });
    }
    /**
     * Sets the idea.
     */
    setIdea(e) {
        this.setState({ idea: e.target.textContent });
    }
    /**
     * Sync data
     *
     * @param      {<type>}             event   The event
     * @param      {(Function|string)}  ref     The reference
     * @param      {<type>}             state   The state
     */
    syncData(event, ref, state) {
        if (event.target.classList.contains('btn')) {
            event.target.classList.add('sync')
        }
        Utils.firebase().ref(Utils.user('uid') + '/' + ref).set(state)
            .then(() => {
                Utils.sleep(1000)
                    .then(() => {
                        if (event.target.classList.contains('sync')) {
                            event.target.classList.remove('sync')
                        }
                    })
            })
    }
    /**
     * Sync note
     */
    syncNote(e) {
        this.syncData(e, 'note', this.state.note);
    }
    /**
     * Sync idea
     */
    syncIdea(e) {
        this.syncData(e, 'idea', this.state.idea);
    }
    /**
     * On mount
     */
    componentDidMount() {
        if (Utils.user('uid')) {

            // get note
            Utils.firebase().ref(Utils.user('uid') + '/note').on('value', item => {
                let note = item.val();
                this.setState({ note: note });
            });

            // get idea
            Utils.firebase().ref(Utils.user('uid') + '/idea').on('value', item => {
                let note = item.val();
                this.setState({ idea: note });
            });

            // get public snippets
            Utils.firebase().ref().on('value', item => {
                let data = item.val();
                let arr = [];
                for (let item in data) {
                    let snipt = data[item]['snippets'];
                    console.log(item.length)
                    Object.entries(snipt).map(item => {
                        if (item[1]['ispublic']) {
                            let data = {
                                id: item[0], // id of snippet
                                uid: item[1].uid, // id of user
                                title: item[1].title,
                                desc: item[1].desc,
                                author: item[1].author
                            }
                            arr.push(data);
                        }
                    })
                }
                arr = Utils.shuffle(arr)
                if (arr.length > 10) arr.length = 10;
                this.setState({ publicSnippets: arr });
            });



        }
    }
    render({},{idea,note,publicSnippets}){
        return (
            <div id="content" class="main-content">
                <div class="nav-header">
                  <nav>
                    <span class="pull-left">
                      <button onClick={() => this.toggleSidebar()} class="o-list" title="Open list"><i class="icon icon-arrow"></i></button>
                    </span>
                    <span class="pull-right">
                      <button onClick={() => Utils.logout()} title="Logout"><i class="icon icon-lock"></i></button>
                      <button onClick={() => this.toggleInfo()}> <i class="icon icon-info"></i></button>
                    </span>
                  </nav>
                </div>
                <div class="content" style="padding:0.5em;">
                    <div class="boxes">
                      <div class="box">
                          <div class="card">
                              <div class="card-title">
                                <span class="pull-left">
                                   {Utils.lang('notes')}
                                </span>
                                <span class="pull-right">
                                    <button class="btn" onClick={(e) => this.syncNote(e)}><i class="icon icon-sync"></i></button>
                                </span>
                              </div>
                              <div class="card-body" contenteditable onInput={(e) => this.setNote(e)}>
                                {note}
                              </div>
                          </div>
                      </div>
                      <div class="box">
                          <div class="card">
                              <div class="card-title">
                                <span class="pull-left">
                                   {Utils.lang('ideas')}
                                </span>
                                <span class="pull-right">
                                    <button class="btn" onClick={(e) => this.syncIdea(e)}><i class="icon icon-sync"></i></button>
                                </span>
                              </div>
                              <div class="card-body" contenteditable onInput={(e) => this.setIdea(e)}>
                                {idea}
                              </div>
                          </div>
                      </div>
                    </div>

                    <div class="boxes">
                      <div class="box">
                          <div class="card">
                              <div class="card-title">
                                <span class="pull-left">{Utils.lang('public_snippets')}</span>
                              </div>
                              <div class="card-body no-padding">
                                   {publicSnippets.map(item =>{
                                        return(
                                          <a
                                            class="public_snippet"
                                            href="#"
                                            onClick={() => this.getSection('/view/'+item.uid+'/'+item.id)}
                                            title={item.title}>
                                            <div class="public_snippet_title">{item.title}</div>
                                            <div class="public_snippet_body"><b>{Utils.lang('author')}: </b>{item.author}</div>
                                          </a>
                                        )
                                    })}
                              </div>
                          </div>
                      </div>
                      <div class="box">
                          <div class="card">
                              <div class="card-title">Example title</div>
                              <div class="card-body"></div>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="main-footer" id="footer"></div>
            </div>
        );
    }
}