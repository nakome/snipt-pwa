import { Component } from 'preact';
import { route } from 'preact-router';

export default class Redirect extends Component {
  /**
   * On mount
   */
  componentWillMount() {
    route(this.props.to);
  }
  /**
   * Render
   */
  render() {
    return null;
  }
}