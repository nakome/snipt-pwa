import { h, render, Component } from 'preact';
import Match from 'preact-router/match';
import { route } from 'preact-router';

// utils
import Utils from '../controllers/utils';

// components
import ContentNavHeader from '../components/contentNavHeader';
import ContentFooter from '../components/contentfooter';


export default class Preview extends Component {
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {}
    };
    /**
     * Gets the section.
     */
    getSection(el) {
        return route(el);
    }
    /**
     * Toggle info
     */
    toggleInfo() {
        Utils.$('.wrapper').classList.toggle('info-is-visible')
    }
    /**
     * Toggle sidebar
     */
    toggleSidebar() {
        Utils.$('.wrapper').classList.toggle('list-is-visible');
    }
    /**
     * Receive props
     */
    componentWillReceiveProps(nextProps) {
        Utils.firebase().ref(nextProps.uid + '/snippets/' + nextProps.id).on('value', item => {
            let data = item.val();
            this.setState(data);
            this.setState({ content: Utils.lang('infoLoaded') });
            Utils.sleep(500)
                .then(() => {
                    if (data) this.setState({ content: marked(data.content) });
                })
        });
    }
    /**
     * On mount
     */
    componentDidMount() {
        Utils.firebase().ref(this.props.uid + '/snippets/' + this.props.id).on('value', item => {
            let data = item.val();
            this.setState(data);
            this.setState({ content: Utils.lang('infoLoaded') });
            Utils.sleep(500)
                .then(() => {
                    if (data) {
                        this.setState({ content: marked(data.content) });
                        Utils.sleep(100).then(() => {
                            Utils.shortCodeParse(Utils.$('.content'));
                        });
                    }
                })
        });
    }
    render({},{title,author,content,date,category}){
        return (
            <div id="content" class="main-content">
                <div class="nav-header">
                  <nav>
                    <span class="pull-left">
                      <button onClick={() => this.toggleSidebar()} class="o-list" title="Open list"><i class="icon icon-arrow"></i></button>
                    </span>
                    <span class="pull-right">
                        <button onClick={() => this.getSection('/')}><i class="icon icon-home"></i></button>
                        <button onClick={() => Utils.logout()}><i class="icon icon-lock"></i></button>
                    </span>
                  </nav>
                </div>
                <div class="content">
                    <h3>{title}</h3>
                    <p><b>{Utils.lang('author')}:  </b> {author}</p>
                    <div dangerouslySetInnerHTML={{ __html: content }}/>
                </div>
                <ContentFooter  category={category} date={date}/>
            </div>
        );
    }
}