// import h & component for class
import { h,Component} from 'preact';
// hendler for inputs
import linkState from 'linkstate';
// import route
import {route} from 'preact-router';
// utils
import Utils from '../controllers/utils';
import {editor_options,config_languages } from '../config.js';
// gets footer component
import ContentFooter from '../components/contentfooter'

/**
 * Class for edit file.
 *
 * @class      EditFile (name)
 */
export default class Edit extends Component{
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {
            uid: Utils.user('uid'),
            category: 'Markdown',
            content: 'Write Something Awesome',
            date: Utils.getFullDate(),
            desc: Utils.lang('A litle description of Snippet'),
            title: '',
            author: Utils.user('displayName'),
            ispublic: false
        }
    }
    /**
     * On mount
     */
    componentDidMount() {
        if (Utils.$('#editorContainer')) {
            let div = document.createElement('div');
            div.id = 'editor';
            Utils.$('#editorContainer').appendChild(div);
            if (div) {
                let editor = ace.edit("editor");
                editor.setOptions(editor_options);
                Utils.sleep(500).then(() => {
                    editor.on("change", (item, e) => {
                        this.setState({ content: editor.getValue() });
                    });
                })
            }
        }
    }
    /**
     * Cancel data
     * Go to home
     */
    cancelData() {
        route('/');
    }


    /**
     * Saves a data.
     *
     * @param      {<type>}   e       { parameter_description }
     * @return     {boolean}  { description_of_the_return_value }
     */
    saveData(e) {
        e.preventDefault();
        if (this.state.title !== '') {
            Utils.firebase().ref(Utils.user('uid') + '/snippets').push(this.state).then((item) => {
                route('/redirect', true);
                Utils.message(Utils.lang('success'), Utils.lang('success_added'));
            }).catch(err => {
                Utils.message(Utils.lang('error'), err.message);
            })
        } else {
            Utils.message(Utils.lang('error'), Utils.lang('error_empty'));
        }
    }
    /**
     * Gets the section.
     *
     * @param      {<type>}  el      { parameter_description }
     * @return     {<type>}  The section.
     */
    getSection(el) {
        return route(el);
    }
    /**
     * Toggle sidebar
     */
    toggleSidebar() {
        Utils.$('.wrapper').classList.toggle('list-is-visible');
    }
    /**
     * Toggle info
     */
    toggleInfo() {
        Utils.$('.wrapper').classList.toggle('info-is-visible')
    }
    /**
     * Render
     */
    render({},{title,desc,category,ispublic = false,date = Utils.getFullDate(),content}){
        return(
            <div id="content" class="main-content">
                  <div class="nav-header">
                      <nav>
                        <span class="pull-left">
                          <button onClick={() => this.toggleSidebar()} class="o-list" title="Open list">
                            <i class="fa fa-arrow-right"></i></button>
                        </span>
                        <span class="pull-right">
                          <button onClick={() => this.getSection('/')}>
                              <i class="icon icon-home"></i>
                          </button>
                          <button onClick={() => Utils.logout()}>
                              <i class="icon icon-lock"></i>
                          </button>
                          <button onClick={() => this.toggleInfo()}>
                            <i class="icon icon-info"></i>
                          </button>
                        </span>
                      </nav>
                  </div>
                <div class="content">

                  <form method="post" class="padded-more">
                    <div class="boxes">
                      <div class="box">
                        <div class="form-group">
                          <label>{Utils.lang('title')}</label>
                          <input
                            type="text"
                            class="form-control"
                            name="title"
                            value={title}
                            required
                            onChange={linkState(this, 'title')}/>
                        </div>
                      </div>
                      <div class="box">
                        <div class="form-group">
                          <label>{Utils.lang('description')}</label>
                          <input
                            type="text"
                            class="form-control"
                            name="desc"
                            required
                            value={desc}
                            onChange={linkState(this, 'desc')}/>
                        </div>
                      </div>
                      <div class="box">
                        <div class="form-group">
                          <label>{Utils.lang('category')}</label>
                          <select name="category"
                            class="form-control"
                            value={category}
                            onChange={linkState(this, 'category')}>
                               {config_languages.map(item =>{
                                 return(<option value={item}>{item}</option>);
                               })}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="boxes">
                       <div class="box">
                          <div id="editorContainer"></div>
                       </div>
                    </div>
                    <div class="boxes">
                       <div class="box">
                          <label>
                              <input type="checkbox" name="ispublic" value={ispublic} onChange={linkState(this, 'ispublic')}/>
                               {Utils.lang('public')}
                          </label>
                       </div>
                    </div>
                    <div class="boxes">
                      <div class="form-actions">
                        <button class="btn btn-form btn-danger" onClick={()=> this.cancelData()}>
                          {Utils.lang('cancel')}
                        </button>
                        <button type="submit"  class="btn btn-form" onClick={e => this.saveData(e)}>
                          {Utils.lang('save')}
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                <ContentFooter category="" date=""/>
            </div>

        );
    }
}