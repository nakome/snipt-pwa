// languages
export const config_lang = {
    en: {
        search: 'Search',
        infoLoaded: 'Loading',
        'Login with Gmail': 'Login with Gmail',
        'Login with Github': 'Login with Github',
        title: 'Title',
        description: 'Description',
        'A litle description of Snippet': 'A litle description of Snippet',
        category: 'Category',
        public: 'Public',
        cancel: 'Cancel',
        save: 'Save',
        delete: 'Delete',
        success: 'Success',
        error: 'Error',
        success_added: 'The Data has been added',
        success_update: 'The Data has been updated',
        success_delete: 'The Data has been deleted',
        error_empty: 'Please write a title',
        publishedOn: 'Published On',
        notes: 'Notes',
        ideas: 'Ideas',
        sync: 'Sync',
        refresh_for_changes: 'Reload for changes',
        language: 'Language',
        english: 'English',
        spanish: 'Spanish',
        color: 'Color',
        dark: 'Dark',
        light: 'Light',
        settings: 'Settings',
        offline: 'You are offline',
        public_snippets: 'Public snippets',
        author: 'Author'
    },
    es: {
        search: 'Buscar',
        infoLoaded: 'Cargando',
        'Login with Gmail': 'Entrar con Gmail',
        'Login with Github': 'Entrar con Github',
        title: 'Titulo',
        description: 'Descripcción',
        'A litle description of Snippet': 'Una pequeña descripcciòn',
        category: 'Categoria',
        public: 'Publico',
        cancel: 'Cancelar',
        save: 'Guardar',
        delete: 'Borrar',
        success: 'Exitó',
        error: 'Error',
        success_added: 'Los datos han sido añadiros',
        success_update: 'Los datos han sido actualizados',
        success_delete: 'Los datos han sido borrados',
        error_empty: 'Por favor escriba el titulo',
        publishedOn: 'Publicado en',
        notes: 'Notas',
        ideas: 'Ideas',
        sync: 'Sync',
        refresh_for_changes: 'Recargar para ver los cambios',
        language: 'Lenguaje',
        english: 'Ingles',
        spanish: 'Español',
        color: 'Color',
        dark: 'Oscuro',
        light: 'Claro',
        settings: 'Ajustes',
        offline: 'Usted esta offline',
        public_snippets: 'Snippets publicos',
        author: 'Autor'
    }
}
// firebase config
export const config_firebase = {
    apiKey: "AIzaSyBdiOIyL7-EUMP-JZO0dL6K4Pm-WaZEoYY",
    authDomain: "snip-42d87.firebaseapp.com",
    databaseURL: "https://snip-42d87.firebaseio.com",
    projectId: "snip-42d87",
    storageBucket: "snip-42d87.appspot.com",
    messagingSenderId: "957061870805"
}
// editor options
export const editor_options = {
    mode: 'ace/mode/markdown',
    theme: 'ace/theme/chrome',
    showGutter: false,
    wrap: true,
    showPrintMargin: false,
    useSoftTabs: false,
    fontSize: 14,
    showInvisibles: false,
    behavioursEnabled: false,
    tabSize: 2,
    useWrapMode: true
}

export const config_settings = {
    theme: 'dark',
    lang: 'en'
}

export const config_languages = ['Angular', 'BatchFile', 'Clojure', 'Cobol', 'Css', 'Dart', 'Ejs', 'Gitignore', 'Haml', 'Handlebars', 'Html', 'Hyperapp', 'Jade', 'Java', 'JavaScript', 'jQuery', 'Json', 'Jsx', 'Julia', 'LaTeX', 'Less', 'Liquid', 'LiveScript', 'Lua', 'Makefile', 'Markdown', 'MySQL', 'Node', 'Npm', 'ObjectiveC', 'Pascal', 'Perl', 'Preact', 'pgSQL', 'Php', 'Powershell', 'Python', 'React', 'Redom', 'Ruby', 'Sass', 'Scala', 'Scss', 'Smarty', 'Snippets', 'Sql', 'SQLServer', 'Stylus', 'Svg', 'Swift', 'Text', 'Textile', 'Twig', 'Typescript', 'VBScript', 'Velocity', 'Xml', 'XQuery', 'Yaml', 'Django'];