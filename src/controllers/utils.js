import Deferred from './deferred';
import { config_firebase, config_settings, config_lang } from '../config.js';
import { route } from 'preact-router';
import
Shortcode from './shortcodes';


// firebase config
const config = config_firebase;
firebase.initializeApp(config);

// google auth
const GoogleProvider = new firebase.auth.GoogleAuthProvider();
const GithubProvider = new firebase.auth.GithubAuthProvider();
const currentUser = 'firebase:authUser:' + config_firebase.apiKey + ':[DEFAULT]';

/**
 * Class for utilities.
 *
 * @class      Utils (name)
 */
class Utils {

    /**
     * Constructs the object.
     *
     * @param  array  options  The options
     */
    constructor(data) {
        this.data = {}
        data = (data) ? data : this.data;
    };

    $(el){
        return document.querySelector(el);
    }

    doWhichKey(e) {
      e = e || window.event;
      var charCode = e.keyCode || e.which;
      return String.fromCharCode(charCode);
    }


    shuffle(array) {
      let currentIndex = array.length, temporaryValue, randomIndex;
      // While there remain elements to shuffle...
      while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    }



    /**
     * { function_description }
     *
     * @param      {<type>}  name    The name
     * @return     {<type>}  { description_of_the_return_value }
     */
    lang(name) {
        let d = 'en';
        if (window.localStorage.getItem('language')) {
            d = window.localStorage.getItem('language');
        } else {
            d = config_settings.lang;
            window.localStorage.setItem('language', d);
        }
        return config_lang[d][name];
    }
    /**
     * Loads a script.
     *
     * @param      {<type>}  s       { parameter_description }
     * @return     {<type>}  { description_of_the_return_value }
     */
    loadScript(s) {
        let d = Deferred();
        let ref = document.getElementsByTagName("script")[0];
        let script = document.createElement("script");
        script.src = s;
        script.async = true;
        ref.parentNode.insertBefore(script, ref);
        script.onload = () => d.resolve();
        return d.promise;
    }
    /**
     * Gets the full date.
     *
     * @return     {Date}  The full date.
     */
    getFullDate() {
        let date = new Date();
        let fullDate = date.getDay() + '/' + date.getMonth() + '/' + date.getFullYear();
        return fullDate;
    }
    /**
     * Firebase
     *
     * @return  fn
     */
    firebase() {
        return firebase.database();
    };

    googleSignin() {
        firebase.auth()
            .signInWithPopup(GoogleProvider).then(result => {
                route('/redirect');
                window.location.reload(true);
            }).catch(error => {
                this.message(this.lang('error'), error.message);
            });
    };
    githubSignin() {
        firebase.auth()
            .signInWithPopup(GithubProvider).then(result => {
                route('/redirect');
                window.location.reload(true);
            }).catch(error => {
                this.message(this.lang('error'), error.message);
            });
    };
    logout() {
        firebase.auth().signOut()
            .then(() => {
                route('/redirect');
                window.location.reload(true);
            });
    };

    user(arr) {
        let user = window.localStorage.getItem(currentUser);
        if (user) {
            user = JSON.parse(user);
            if (user) {
                return user[arr];
            }
        }
    };

    userUid() {
        let user = this.user('uid');
        return user;
    };

    isLogged() {
        let auth = false;
        if (window.localStorage.getItem(currentUser)) {
            auth = true;
        }
        return auth;
    };
    /**
     * Capitalize
     *
     * @param      {string}  string  The string
     * @return     {string}  { description_of_the_return_value }
     */
    capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    /**
     * Sleep
     *
     * @param      {<type>}   time    The time
     * @return     {Promise}  { description_of_the_return_value }
     */
    sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    };
    /**
     * Determines if mobile.
     */
    isMobile() {
        // device detection
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;
    };
    /**
     * Message
     *
     * @param      {string}  title   The title
     * @param      {<type>}  msg     The message
     */
    message(title, msg) {
        let messageAdd = document.createElement('div');
        if (title === 'Error') messageAdd.className = 'alert alert-error notification';
        else messageAdd.className = 'alert alert-success notification';
        messageAdd.innerHTML += `<p><b>${title}</b>, ${msg}</p>`;
        document.body.appendChild(messageAdd);
        navigator.vibrate([100, 50, 100]);
        var w = setTimeout(() => {
            messageAdd.className = 'alert hide-notification';
            var w2 = setTimeout(() => {
                document.body.removeChild(messageAdd);
                clearTimeout(w2);
            }, 500);
            clearTimeout(w);
        }, 5000);
    };
    /**
     * Notifies a me.
     *
     * @param      {<type>}  msg     The message
     */
    notifyMe(title, msg) {
        if (!this.isMobile) {
            if (Notification.permission === 'granted') {
                let notification = new Notification(title, {
                    icon: 'assets/icon.png',
                    body: msg,
                });
                navigator.vibrate([100, 50, 100]);
                this.sleep(3000).then(() => {
                    notification.close();
                });
            }
        } else {
            Notification.requestPermission(result => {
                if (result === 'granted') {
                    navigator.serviceWorker.ready.then(registration => {
                        registration.showNotification(msg);
                        navigator.vibrate([100, 50, 100]);
                    });
                }
            });
        }
    }

    shortCodeParse(root){
      new Shortcode(root, {
        script(done){
            return '<script rel="javascript">'+eval(this.contents)+'</script>';
        },
        style(done){
            return '<style rel="stylesheet">'+this.contents+'</style>';
        },
        note(done) {
          return this.options ? '<div class="note ' + this.options.type + '">' + this.contents + "</div>" : '<div class="note primary">' + this.contents + "</div>";
        },
        lblk(done) {
          return '<div class="boxes">\n<div class="box">\n<img src="//' + (this.options.img ? this.options.img : "") + '"/>\n</div>\n<div class="box">\n' + (this.contents ? this.contents : "") + "\n</div>\n</div>";
        },
        rblk(done) {
          var r = this.options.img ? this.options.img : "";
          return '<div class="boxes">\n<div class="box">\n' + (this.contents ? this.contents : "") + '\n</div>\n<div class="box">\n<img src="//' + r + '"/>\n</div>\n</div>';
        },
        vimeo(done) {
          return '<section class="iframe">\n<iframe src="https://player.vimeo.com/video/' + this.options.id + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n</section>';
        },
        youtube(done) {
          return '<section class="iframe">\n<iframe src="https://www.youtube.com/embed/' + this.options.id + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n</section>';
        },
        video(done) {
          return '<video class="iframe" src="//' + this.options.url + '" controls></video>'
        },
        iframe(done) {
          return '<section class="iframe">\n <iframe src="//' + (this.options.src ? this.options.src : "example.com") + '"></iframe>\n</section>';
        },
        codepen(done) {
          return '<section class="iframe">\n<iframe scrolling src=\'//codepen.io/' + this.options.id + "/?height=500' frameborder allowtransparency allowfullscreen></iframe>\n</section>";
        }
      })
    }
}

export default new Utils();