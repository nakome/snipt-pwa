/**
 * Deferred promis
 *
 * @class      Deferred (name)
 * @return     {<type>}  { description_of_the_return_value }
 */
const Deferred = () => {
        let d = {};
        let promise = new Promise((resolve, reject) => {
            d.resolve = resolve;
            d.reject = reject;
        });

        d.promise = promise;
        return Object.assign(d, promise);
    };

export default Deferred;
