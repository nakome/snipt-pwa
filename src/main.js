import * as OfflinePluginRuntime from 'offline-plugin/runtime';
OfflinePluginRuntime.install();

import './style.css';
import {h,render,Component } from 'preact';
import Match from 'preact-router/match';
import {Router,Route,route} from 'preact-router';
import Utils from './controllers/utils.js';

// components
import NavTags from './components/navtags';
import NavInfo from './components/navinfo';
import Sidebar from './components/sidebar';

// views
import Home from './views/home';
import Preview from './views/preview';
import Public from './views/public';
import Edit from './views/edit';
import New from './views/new';
import Redirect from './views/redirect';
import Error404 from './views/error'


// granted databasse
if (Notification.permission !== 'granted')  Notification.requestPermission();


class App extends Component {
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {
            data: [],
            public: []
        }
    }
    /**
     * Loads a data.
     */
    loadData() {
        Utils.firebase().ref(Utils.user('uid') + '/snippets').on('value', item => {
            let data = item.val();
            this.setState({ data: data });
        });
        //info changes
        Utils.firebase().ref(Utils.user('uid') + '/snippets').on('child_removed', item => {
            Utils.notifyMe('Success', 'Data has been remove');
        });
    }
    /**
     * On Mount
     */
    componentDidMount() {
        this.loadData();
        let overlay = document.querySelector('.overlay');
        if (overlay) {
            overlay.addEventListener('click', e => {
                e.preventDefault();
                document.querySelector('.wrapper').className = 'wrapper';
            });
        }

        let themeColor = window.localStorage.getItem('theme_color');
        if (themeColor) {
            document.body.classList.add(themeColor);
        }
    }
    /**
     * Handle route
     */
    handleRoute(e) {
        let { onChange } = this.props;
        if (onChange) onChange(e);
    };
    /**
     * Render
     */
    render({onChange},{data}){
      if(Utils.isLogged()){
        return(<div class="wrapper" id="root">
                  <NavTags />
                  <main class="main">
                      <Sidebar data={data}/>
                      <Router onChange={(e) =>this.handleRoute(e)}>
                        <Route path="/" component={Home}/>
                        <Route path="/view/:uid/:id" component={Public} />
                        <Route path="/view/:uid" component={Preview} />
                        <Route path="/edit/:uid" component={Edit}/>
                        <Route path="/new" component={New}/>
                        <Route path="/redirect"  to="/" component={Redirect} />
                        <Route component={Error404} default />
                      </Router>
                      <div class="overlay"></div>
                  </main>
                  <NavInfo />
              </div>);
      }else{
        return(<div class="wrapper" id="root">
                  <main class="main">
                      <div id="content">
                        <div class="boxes text-center">
                          <div class="box" style="padding-top:3em;">
                            <button class="btn" onClick={() => Utils.googleSignin()}>
                              {Utils.lang('Login with Gmail')}
                            </button>
                          </div>
                        </div>
                      </div>
                  </main>
              </div>);
      }
    }
}




// register the service worker if available
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./sw.js').then(function(reg) {
        console.log('Successfully registered service worker', reg);
    }).catch(function(err) {
        console.warn('Error whilst registering service worker', err);
    });
}

window.addEventListener('online', function(e) {
    // re-sync data with server

}, false);

window.addEventListener('offline',e => {
    // queue up events for server
    console.log("You are offline");
    Utils.message(Utils.lang('error'),Utils.lang('offline'));
}, false);


// check if the user is connected
if (navigator.onLine) {
    const root = document.body;
    render(<App />, root);
}
