// import  h & component for class
import { h,Component } from 'preact';
// import route for links
import {route} from 'preact-router';
import Utils from '../controllers/utils';


export default class ContentNavHeader extends Component{

  /**
   * Gets the section.
   *
   * @param      {<type>}  id      The identifier
   */
  getSection(el){
      return route(el, true);
  }
  /**
   * Saves a data.
   *
   * @param      {<type>}   e       { parameter_description }
   * @return     {boolean}  { description_of_the_return_value }
   */
  removeFile(id) {
      Utils.firebase().ref(Utils.user('uid') + '/snippets/' + id).remove()
      .then(() => {
          route('/redirect');
          Utils.message(Utils.lang('success'), Utils.lang('success_delete'));
      }).catch(error => {
          Utils.message(Utils.lang('error'), error.message);
      });
  };
  /**
   * Toggle sidebar
   */
  toggleSidebar(){
     Utils.$('.wrapper').classList.toggle('list-is-visible');
  }
  /**
   * Toggle info
   */
  toggleInfo(){
   Utils.$('.wrapper').classList.toggle('info-is-visible')
  }
  /**
   * render
   */
  render({uid},{}){
    return(
      <div class="nav-header">
          <nav>
            <span class="pull-left">
              <button onClick={() => this.toggleSidebar()} class="o-list"><i class="icon icon-arrow"></i></button>
            </span>
            <span class="pull-right">
              <button onClick={() => this.getSection('/')}><i class="icon icon-home"></i></button>
              <button onClick={() => this.getSection('/edit/'+uid)}><i class="icon icon-edit"></i></button>
              <button onClick={() => {
                 if(confirm('Are you sure ?')){
                  this.removeFile(this.props.uid)};
                }
              } title="Delete item"><i class="icon icon-trash"></i></button>
              <button onClick={() => Utils.logout()} title="Logout"><i class="icon icon-lock"></i></button>
              <button onClick={() => this.toggleInfo()}> <i class="icon icon-info"></i></button>
            </span>
          </nav>
      </div>
    );
  }
}