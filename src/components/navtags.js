// import  h & component for class
import { h,Component } from 'preact';
// import route for links
import {route} from 'preact-router';
import Utils from '../controllers/utils';
export default class NavTags extends Component{
  /**
   * Toggle tags
   */
  toggleTags(e){
    e.preventDefault();
    Utils.$('.wrapper').classList.toggle('tags-is-visible')
  }
  /**
   * Change language
   */
  changeLanguage(e){
    window.localStorage.setItem('language',e.target.value);
    Utils.message(Utils.lang('success'),Utils.lang('refresh_for_changes'));
  }
  /**
   * Change theme
   */
  changeTheme(e){
    window.localStorage.setItem('theme_color',e.target.value);
    document.body.className = e.target.value;
  }
  /**
   * Render
   */
  render(){
    return(
      <div class="nav-tags" id="nav-tags">
          <div class="nav-header">
              <nav>
                  <span class="pull-left"><h3>{Utils.lang('settings')}</h3></span>
                  <span class="pull-right">
                    <button onClick={(e) => this.toggleTags(e)} class="close-tags">
                      <i class="icon icon-close"></i>
                    </button>
                  </span>
              </nav>
          </div>
          <div class="tags-content">
              <label>{Utils.lang('language')}</label>
              <select name="language" onChange={(e) => this.changeLanguage(e)}>
                  <option value="en">{Utils.lang('english')}</option>
                  <option value="es">{Utils.lang('spanish')}</option>
              </select>
              <label>Theme {Utils.lang('color')}</label>
              <select name="language" onChange={(e) => this.changeTheme(e)}>
                  <option>-- Select --</option>
                  <option value="dark">{Utils.lang('dark')}</option>
                  <option value="light">{Utils.lang('light')}</option>
              </select>
          </div>
      </div>
    );
  }
}