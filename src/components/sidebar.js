// import  h & component for class
import { h,Component } from 'preact';
// import route for links
import {route} from 'preact-router';
import Utils from '../controllers/utils'

/**
 * Class for sidebar.
 *
 * @class      Sidebar (name)
 */
export default class Sidebar extends Component {
    /**
     * Constructs the object.
     */
    constructor() {
        super();
        this.state = {
            loaded:false,
            infoLoaded: Utils.lang('infoLoaded'),
            data:[]
        }
    };

    /**
     * Component di mount
     */
    componentWillReceiveProps(nextProps) {
         let data = nextProps.data;
         if(data){
            Utils.sleep(500)
             .then(()=>{
                this.setState({loaded:true,data: data});
             })
         }else{
            this.setState({loaded:true});
         }
    };
    /**
     * Gets the section.
     *
     * @param      {<type>}  id      The identifier
     */
    getSection(el){
        this.toggleSidebar();
        return route(el);
    }
    /**
     * Searches for the first match.
     */
    search(e){
        let event = e.target.value;
        if(event.length > 2){
            // search in titles
            Utils.firebase()
            .ref(Utils.user('uid')+'/snippets')
            .orderByChild("title")
            .startAt(Utils.capitalize(event))
            .endAt(Utils.capitalize(event)+"\uf8ff")
            .on('value', item =>{
                if(item.val()) this.setState({data:item.val()});
            });
            // search in category
            Utils.firebase()
                .ref(Utils.user('uid')+'/snippets')
                .orderByChild("category")
                .equalTo(event)
                .on('value', item =>{
                if(item.val()) this.setState({data:item.val()});
            });
        }else{
            // default
            Utils.firebase()
                .ref(Utils.user('uid')+'/snippets')
                .on('value', item =>{
                    if(item.val()) this.setState({data:item.val()});
            });
        }
    }
    /**
     * Toggle tags
     */
    toggleTags(e){
        e.preventDefault();
        Utils.$('.wrapper').classList.toggle('tags-is-visible')
    }
    /**
     * Toggle sidebar
     */
    toggleSidebar(){
       Utils.$('.wrapper').classList.toggle('list-is-visible');
    }
    /**
     * Render
     */
    render({},{data,infoLoaded}){
        return(<div id="sidebar" class="nav-list">
            <div class="nav-header">
                <nav>
                    <button onClick={(e) => this.toggleTags(e)}><i class="icon icon-tags"></i></button>
                    <input type="search" onInput={(e) => this.search(e)} placeholder={Utils.lang('search')}/>
                    <button onClick={(e) => this.getSection('/new')}><i class="icon icon-plus"></i></button>
                    <span class="pull-right">
                       <button onClick={() => this.toggleSidebar()} class="o-list"><i class="icon icon-close"></i></button>
                    </span>
                </nav>
            </div>
            <ul class="nav-menu" id="list">
               {   (this.state.loaded) ?
                    Object.entries(data).map(item =>{
                        // random color
                        const randColor = "#"+((1<<24)*Math.random()|0).toString(16);
                        return(
                            <li> <span class={`tag tag-${item[1].category}`} style={`background-color: ${randColor}`}></span>
                                <a href="#" onClick={() => this.getSection('/view/'+item[0])} title={item[1].category}>
                                    <div class="nav-menu-title">{item[1].title}</div>
                                    <div class="nav-menu-desc">{item[1].desc}</div>
                                </a>
                            </li>
                        )
                    }) : infoLoaded
               }
            </ul>
        </div>)
    }
}