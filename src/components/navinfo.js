// import  h & component for class
import { h,Component } from 'preact';
// import route for links
import {route} from 'preact-router';
// import utils
import Utils from '../controllers/utils';
// export class
export default class NavInfo extends Component{
  /**
   * Toggle info
   */
  toggleInfo(e){
    e.preventDefault();
    Utils.$('.wrapper').classList.toggle('info-is-visible')
  }
  /**
   * On mount
   */
  componentDidMount() {
    if(Utils.isLogged()){;
      this.setState({
          photo: Utils.user('photoURL'),
          username: Utils.user('displayName'),
          useremail: Utils.user('email')
      })
    }
  }
  /**
   * { function_description }
   */
  render({},{photo,username,useremail}){
    return(
      <div class="nav-info" id="nav-info">
          <div class="nav-header">
              <nav>
                  <span class="pull-right">
                    <button  onClick={(e) => this.toggleInfo(e)} class="c-close">
                      <i class="icon icon-close"></i>
                    </button>
                  </span>
              </nav>
          </div>
          <div class="info-content">
              <img src={photo}/>
              <p><b>Name: </b>{username}</p>
              <p><b>Email: </b>{useremail}</p>
          </div>
      </div>
    );
  }
}