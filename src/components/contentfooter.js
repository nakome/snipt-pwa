// import  h & component for class
import { h,Component } from 'preact';
// import route for links
import {route} from 'preact-router';
//import utils
import Utils from '../controllers/utils';
// export class
export default class NavInfo extends Component{
  render(){
    return(
      <div class="main-footer" id="footer">
          <span class="pull-left"><strong>{Utils.lang('category')}: </strong> {this.props.category}</span>
          <span class="pull-right"><strong>{Utils.lang('publishedOn')}: </strong>{this.props.date}</span>
      </div>
    );
  }
}