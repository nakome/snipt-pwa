var fs = require('fs');
var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var OfflinePlugin = require('offline-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');





var loaders = [
  {
    "test": /\.jsx?$/,
    "exclude": /node_modules/,
    "loader": "babel-loader",
    "query": {
      "presets": [
        "babel-preset-es2015"
      ],
      "plugins": [
        [
          "babel-plugin-transform-react-jsx",
          {
            "pragma": "h"
          }
        ]
      ]
    }
  },
  {
    test: /\.css$/,
    use: ExtractTextPlugin.extract({
      fallback: "style-loader",
      use: "css-loader"
    })
  },
  {
    "test": /\.json?$/,
    "loader": "json-loader"
  }
];

module.exports = {
  devtool: 'eval-source-map',
  entry: path.resolve('src', 'main.js'),
  output: {
    path: path.resolve('build'),
    filename: 'main.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  plugins: [
    new ExtractTextPlugin('style.css'),
    new HtmlWebpackPlugin({
      template: path.resolve('src', 'index.tpl.html'),
      filename: 'index.html',
      inject: false
    }),
    // ncp src/manifest.json build/manifest.json && ncp src/assets build/assets && ncp content build/content
    new CopyWebpackPlugin([
      { from: './src/offline.html' },
      { from: './src/manifest.json' },
      { from: './src/assets', to: 'assets' }
    ]),
    new OfflinePlugin(),
    new ProgressBarPlugin()
  ],
  module: {
    loaders: loaders
  },
  devServer: {
    port: process.env.PORT || 3000,
    host: '0.0.0.0',
    publicPath: '/',
    quiet: true,
    clientLogLevel: 'error',
    compress: true,
    historyApiFallback: true,
    setup(app) {
      app.use('/fs/**', (req, res) => {
        fs.createReadStream(`fs/${req.params[0]}`).pipe(res);
      });
    }
  }
};
